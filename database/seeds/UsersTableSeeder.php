<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'      => 'admin',
            'user_name' => 'admin',
            'avatar'    => 'public/user.png',
            'user_role' => 'admin',
            'isVerify'  => 1,
            'otp'       => NULL,
            'email'     => 'admin@gmail.com',
            'password'  => bcrypt('password'),
            'registered_at' => date('Y-m-d H:i:s'),
        ]);

     
        
    }
}
