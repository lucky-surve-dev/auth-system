<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator,File,URL;
use App\Http\Requests;
use App\Mail\SendMail;
use App\Mail\SendInviteMail;
class UserController extends Controller
{
    public $successStatus = 200;

    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password'), 'isVerify' => 1] )){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            return response()->json(['success' => $success], $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
    
    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    
     public function register(Request $request) 
    { 
            $validator = Validator::make($request->all(), [ 
                'name' => 'required', 
                'user_name' => 'required|min:4|max:20', 
                'email' => 'required|email|unique:users,email', 
                'password' => 'required', 
                'c_password' => 'required|same:password', 
            ]);
        
            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }
            $input = $request->all(); 
            $input['user_role'] = 'user';
            $input['password'] = bcrypt($input['password']); 
            $user = User::create($input); 
            $success['name'] =  $user->name;
            $success['user_name'] =  $user->user_name;

            $otp = rand(10000,99999);
            
           
            
            // $contactEmail = $user->email;

            $Isuser = User::where('email','=',$user->email)->update(['otp' => $otp]);

            $data = array('name'=> $user->name, 'otp'=> $otp,'email' => $user->email);


            $IsSent = \Mail::to($data['email'])->send(new SendMail($data));

            $success['message'] = 'OTP(One Time Password) is sucessfully sent on your email.';
            return response()->json(['success'=>$success], $this->successStatus);
           
    }
    
    /** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    
     public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    }

    public function otpverify(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'otp' => 'required',
        ]);
    
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $Isuser = User::where('otp','=',$request->otp)->update(['otp' => null, 'isVerify' => 1 , 'email_verified_at' => date('Y-m-d H:i:s')]);
        

        if($Isuser){
            $success['message'] =  'OTP Verification Successful.';
            return response()->json(['success' => $success], $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Invalid Otp'], 401); 
        } 
    }


    public function updateprofile(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'user_name' => 'required|min:4|max:20',
            'avatar'  => 'required|mimes:jpg,png,jpeg|max:2048|dimensions:min_width=256,min_height=256',
        ]);
        
    
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        if ($files = $request->file('avatar')) {
            $destinationPath = 'file/'; // upload path
            $profilefile = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profilefile);
            $avatar = $profilefile;
         }

         $Isuser = User::where('id','=',$request->id)->update([
            'name'      => $request->name, 
            'user_name' => $request->user_name, 
            'avatar'    => $avatar]
        );
               
        if($Isuser){
            $success['message'] =  'Profile Updated Successfully.';
            return response()->json(['success' => $success], $this->successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Something Went Wrong.'], 401); 
        } 

    }
    
    public function sendinvitations(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email|unique:users,email',
        ]);
        
    
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $link = URL::to('/').'/api/register';

        $data = array('link'=> $link,'email'=>$request->email);

        $IsSent = \Mail::to($data['email'])->send(new SendInviteMail($data));

        $success['message'] =  'Invitation Sent Successfully.';
        return response()->json(['success' => $success], $this->successStatus);
        
    }
}


